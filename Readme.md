1.Download and set protoc into env path

https://github.com/protocolbuffers/protobuf/releases

https://github-production-release-asset-2e65be.s3.amazonaws.com/23357588/33d9c1fc-9410-11e8-85d7-fa4bae109bd6?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWNJYAX4CSVEH53A%2F20180930%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20180930T130543Z&X-Amz-Expires=300&X-Amz-Signature=e714633d9bb053be3c7815c4846b22bc56236177528fb99ab74211d8938c664a&X-Amz-SignedHeaders=host&actor_id=9973689&response-content-disposition=attachment%3B%20filename%3Dprotoc-3.6.1-win32.zip&response-content-type=application%2Foctet-stream

2.Install go dependencies

    a)https://github.com/golang/protobuf

    b)https://github.com/grpc-ecosystem/grpc-gateway
        
            go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
            go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
            go get -u github.com/golang/protobuf/protoc-gen-go

    $GOBIN, defaulting to $GOPATH/bin. It must be in your $PATH for the protocol compiler, protoc, to find it.

3.go get -v -d gitlab.com/chokkar.g/grpc_app

4.To build go code from proto

protoc -I api/  api/api.proto --go_out=plugins=grpc:api

5.Initial Output like:

D:\GoProgramming\src\gitlab.com\chokkar\grpc_app\server>server.exe
2018/09/30 21:36:04 Receive message foo

D:\GoProgramming\src\gitlab.com\chokkar\grpc_app\client>client.exe
2018/09/30 21:36:04 Response from server: bar